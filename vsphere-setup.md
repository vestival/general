# vSphere Setup

There is one vCenter server with 2 CPUs and 4 GB RAM running vCenter 5.5.

Two ESX nodes esxi1 and esxi2 each having 4 CPUs and 16 GB RAM.

NFS storage is mounted from [RH IDM](rhidm-setup.md).
