# Red Hat Identity Manager

Currently this IDM instance is not used for authentication of any other service.

The VM provides NFS services used by [RHV](rhv-setup.md) and [VMware vSphere](vsphere-setup.md).