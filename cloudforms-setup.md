# CloudForms Setup

There are 5 appliances, each in its own dedicated zone. All Zones belong to the same CloudForms Region. The CF46-DB Appliance is the dedicated Database for this region.

## Zone: Web UI

CF46-UI, provides Web UI for the entire CloudForms setup and is the only user interface end users are supposed to directly interact with.

## Zone: CF Infra Providers

CF46-INF, dedicated appliance for the infrastructure provider, [vSphere](vsphere-setup.md) and [RHV](rhv-setup.md).

## Zone: CF OSP Provider

CF46-OSP, only provider in this Zone is [OpenStack](openstack-setup.md).