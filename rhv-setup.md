# RHV Setup

Current Version: 4.2.4
One Manager, two Hypervisors
NFS Storage is mounted from Red Hat Identity Manager Machine, more details about the [IDM Setup](rhidm-setup.md).

RHV-M: 4 CPU, 8 GB RAM
RHV-H1 and RHV-H2: 4 CPU, 16 GB RAM
